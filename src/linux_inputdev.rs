// SPDX-License-Identifier: GPL-2.0-only
/*
 *  Copyright (C) 2021, Richard Weinberger <richard@nod.at>
 */

#![allow(dead_code)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

use std::fs::OpenOptions;
use std::io;
use std::io::Read;
use std::os::unix::fs::OpenOptionsExt;
use std::os::unix::io::AsRawFd;

use crate::poll;

pub trait InputEventRecv {
	fn consume_input(&mut self, code: u32, value: i32) -> io::Result<bool>;
}

pub fn do_events(inputdev: &str, ier: &mut dyn InputEventRecv) -> io::Result<()> {
	let mut input = OpenOptions::new()
		.read(true)
		.custom_flags(libc::O_NONBLOCK)
		.open(&inputdev)?;

	let struct_size = std::mem::size_of::<input_event>();
	let mut buffer = vec![0; struct_size];

	loop {
		let revents = poll::single_poll(input.as_raw_fd(), libc::POLLIN | libc::POLLPRI)?;
		if revents == 0 {
			continue;
		}

		input.read_exact(&mut buffer)?;

		let s: input_event = unsafe { std::ptr::read(buffer.as_ptr() as *const _) };

		let ev_type: u32 = s.type_ as u32;
		if ev_type == EV_KEY {
			match ier.consume_input(s.code as u32, s.value as i32) {
				Ok(v) => {
					if v == false {
						return Ok(());
					}
				}
				Err(e) => {
					return Err(e);
				}
			}
		}
	}
}
