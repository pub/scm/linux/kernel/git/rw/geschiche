// SPDX-License-Identifier: GPL-2.0-only
/*
 *  Copyright (C) 2021, Richard Weinberger <richard@nod.at>
 */

mod config;
mod linux_inputdev;
mod player;
mod playerbackend;
mod playprogram;
mod poll;
mod vlc;

use crate::player::Player;

fn main() {
	let mut player = Player::new().unwrap_or_else(|| {
		eprintln!("Unable to initialize player");
		std::process::exit(1);
	});

	let input_file = player
		.cfg
		.base
		.get("inputdev")
		.unwrap_or_else(|| {
			eprintln!("No inputdev in base section configured!");
			std::process::exit(1);
		})
		.clone();

	match linux_inputdev::do_events(&input_file, &mut player) {
		Err(e) => {
			eprintln!("Error while processing input events: {}", e);
			std::process::exit(1);
		}
		Ok(_) => {
			std::process::exit(0);
		}
	}
}
