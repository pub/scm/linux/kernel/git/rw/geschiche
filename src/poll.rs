// SPDX-License-Identifier: GPL-2.0-only
/*
 *  Copyright (C) 2021, Richard Weinberger <richard@nod.at>
 */

use std::io;
use std::os::unix::io::RawFd;

pub fn single_poll(fd: RawFd, events: i16) -> io::Result<i16> {
	let mut pfd = libc::pollfd {
		fd: fd,
		events: events,
		revents: 0,
	};

	let pret;
	unsafe {
		pret = libc::poll(&mut pfd, 1 as u64, -1);
	};

	if pret < 0 {
		Err(io::Error::from_raw_os_error(-pret))
	} else if pret == 0 {
		Ok(0)
	} else {
		Ok(pfd.revents)
	}
}
