// SPDX-License-Identifier: GPL-2.0-only
/*
 *  Copyright (C) 2021, Richard Weinberger <richard@nod.at>
 */

use std::io;
use std::io::Read;
use std::io::Write;
use std::net::TcpStream;
use std::os::unix::io::AsRawFd;
use std::path::PathBuf;

use crate::config::PlayerConfig;
use crate::playerbackend::PlayerBackend;
use crate::poll;

pub struct BackendVlc {
	conn: std::net::TcpStream,
	volume: u32,
	volume_step: u32,
}

impl BackendVlc {
	pub fn new(cfg: &PlayerConfig) -> Option<BackendVlc> {
		let vlcaddr = match cfg.backendconfig.get("addr") {
			None => {
				eprintln!("addr not set in backendconfig section!");
				return None;
			}
			Some(v) => v,
		};

		match BackendVlc::init(vlcaddr) {
			Ok(vlc) => Some(vlc),
			Err(e) => {
				eprintln!("Unable to connect to VLC: {}", e);
				None
			}
		}
	}

	fn init(dst: &str) -> io::Result<BackendVlc> {
		let conn = TcpStream::connect(dst)?;

		conn.set_nonblocking(true)?;

		let mut vlc = BackendVlc {
			conn: conn,
			volume: 256,
			volume_step: 5,
		};

		vlc.wait_till_ready()?;
		vlc.do_vlc_cmds(vec![String::from("stop"), String::from("clear")])?;
		vlc.set_volume(vlc.volume)?;

		Ok(vlc)
	}

	fn wait_till_ready(&mut self) -> io::Result<()> {
		let mut rbuf = vec![0; 128];
		let mut total = Vec::new();

		loop {
			let revents = poll::single_poll(self.conn.as_raw_fd(), libc::POLLIN | libc::POLLPRI)?;
			if revents == 0 {
				continue;
			} else if revents as u32 & (libc::POLLERR as u32 | libc::POLLHUP as u32) != 0 {
				return Err(io::Error::new(
					io::ErrorKind::Other,
					"Connection to VLC gone",
				));
			}

			let got = self.conn.read(&mut rbuf)?;

			total = [&total, &rbuf[0..got]].concat();

			let resp = String::from_utf8_lossy(&total);

			if resp.ends_with("> ") {
				break;
			}
		}

		Ok(())
	}

	fn do_vlc_cmds(&mut self, cmds: Vec<String>) -> io::Result<()> {
		for cmd in cmds {
			self.conn.write(format!("{}\n", cmd).as_bytes())?;
			self.wait_till_ready()?;
		}

		Ok(())
	}
}

impl PlayerBackend for BackendVlc {
	fn set_volume(&mut self, vol: u32) -> io::Result<()> {
		self.do_vlc_cmds(vec![format!("volume {}", vol)])
	}

	fn volume_up(&mut self) -> io::Result<()> {
		self.do_vlc_cmds(vec![format!("volup {}", self.volume_step)])
	}

	fn volume_down(&mut self) -> io::Result<()> {
		self.do_vlc_cmds(vec![format!("voldown {}", self.volume_step)])
	}

	fn pause(&mut self) -> io::Result<()> {
		self.do_vlc_cmds(vec![String::from("pause")])
	}

	fn stop(&mut self) -> io::Result<()> {
		self.do_vlc_cmds(vec![String::from("stop")])
	}

	fn play(&mut self) -> io::Result<()> {
		self.do_vlc_cmds(vec![String::from("play")])
	}

	fn play_single_file(&mut self, file: &PathBuf) -> io::Result<()> {
		self.play_multiple_files(vec![file])
	}

	fn play_multiple_files(&mut self, files: Vec<&PathBuf>) -> io::Result<()> {
		let mut cmds = Vec::new();

		cmds.push(String::from("stop"));
		cmds.push(String::from("clear"));

		for file in files {
			let f = file.to_str().unwrap();

			cmds.push(format!("enqueue {}", f));
		}

		cmds.push(String::from("goto 0"));
		cmds.push(String::from("play"));

		self.do_vlc_cmds(cmds)
	}
}
