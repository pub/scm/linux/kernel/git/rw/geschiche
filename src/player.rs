// SPDX-License-Identifier: GPL-2.0-only
/*
 *  Copyright (C) 2021, Richard Weinberger <richard@nod.at>
 */

use std::cell::RefCell;
use std::collections::HashMap;
use std::io;
use std::rc::Rc;
use std::rc::Weak;

use crate::config;
use crate::linux_inputdev;
use crate::playerbackend::PlayerBackend;
use crate::playprogram::{Linear, PlayProgram, Random, Shuffle};
use crate::vlc;

#[derive(Clone)]
enum PlayerAction {
	Play,
	Stop,
	Pause,
	Next,
	Prev,
	VolUp,
	VolDown,
	SwitchProgram(Box<String>),
}

pub struct Player {
	programs: HashMap<String, Rc<RefCell<dyn PlayProgram>>>,
	cur: Option<Weak<RefCell<dyn PlayProgram>>>,
	actions: HashMap<u32, PlayerAction>,
	backend: Box<dyn PlayerBackend>,
	pub cfg: config::PlayerConfig,
}

fn new_player() -> Option<Player> {
	let cfg = match config::init_cfg() {
		None => {
			eprintln!("Unable to continue, config file is missing");
			return None;
		}
		Some(cfg) => cfg,
	};

	let backend_name = match cfg.base.get("backend") {
		None => {
			eprintln!("Unable to continue, no backend configured");
			return None;
		}
		Some(name) => name,
	};

	let backend = match backend_name.as_str() {
		"vlc_rc" => {
			let vlc = vlc::BackendVlc::new(&cfg);

			if vlc.is_none() {
				eprintln!("Unable to continue, backend init failed");
				return None;
			}

			vlc.unwrap()
		}
		_ => {
			eprintln!("Unable to continue, Unknown backend: {}", backend_name);
			return None;
		}
	};

	let mut programs: HashMap<String, Rc<RefCell<dyn PlayProgram>>> = HashMap::new();
	let mut actions: HashMap<u32, PlayerAction> = HashMap::new();

	for (keyaction, keycode) in &cfg.keymap {
		match keyaction.as_ref() {
			"play" => {
				actions.insert(*keycode, PlayerAction::Play);
			}
			"stop" => {
				actions.insert(*keycode, PlayerAction::Stop);
			}
			"pause" => {
				actions.insert(*keycode, PlayerAction::Pause);
			}
			"volup" => {
				actions.insert(*keycode, PlayerAction::VolUp);
			}
			"voldown" => {
				actions.insert(*keycode, PlayerAction::VolDown);
			}
			"next" => {
				actions.insert(*keycode, PlayerAction::Next);
			}
			"prev" => {
				actions.insert(*keycode, PlayerAction::Prev);
			}
			_ => {
				eprintln!("Unknown action: {}", keyaction);
				return None;
			}
		}
	}

	for p in &cfg.playlist {
		match p.program.as_ref() {
			"linear" => {
				let pl = match Linear::new(p) {
					Err(e) => {
						eprintln!("Unable to init linear playlist program {}: {}", p.name, e);
						return None;
					}
					Ok(v) => v,
				};

				let prg = Rc::new(RefCell::new(pl));
				programs.insert(p.name.clone(), prg);
			}
			"shuffle" => {
				let pl = match Shuffle::new(p) {
					Err(e) => {
						eprintln!("Unable to init shuffle playlist program {}: {}", p.name, e);
						return None;
					}
					Ok(v) => v,
				};

				let prg = Rc::new(RefCell::new(pl));
				programs.insert(p.name.clone(), prg);
			}
			"random" => {
				let pl = match Random::new(p) {
					Err(e) => {
						eprintln!("Unable to init random playlist program {}: {}", p.name, e);
						return None;
					}
					Ok(v) => v,
				};

				let prg = Rc::new(RefCell::new(pl));
				programs.insert(p.name.clone(), prg);
			}
			_ => {
				eprintln!("Unknown playlist program program: {}", p.name);
				return None;
			}
		};

		actions.insert(
			p.keycode,
			PlayerAction::SwitchProgram(Box::new(String::from(&p.name))),
		);
	}

	Some(Player {
		programs: programs,
		cur: None,
		actions: actions,
		backend: Box::new(backend),
		cfg: cfg,
	})
}

impl Player {
	pub fn new() -> Option<Player> {
		new_player()
	}

	fn next(&mut self) -> io::Result<()> {
		if let Some(cur_weak) = &self.cur {
			let cur_rc = Weak::upgrade(cur_weak).unwrap();
			let mut cur = cur_rc.borrow_mut();
			cur.next(&mut (*self.backend))?;
		}

		Ok(())
	}

	fn prev(&mut self) -> io::Result<()> {
		if let Some(cur_weak) = &self.cur {
			let cur_rc = Weak::upgrade(cur_weak).unwrap();
			let mut cur = cur_rc.borrow_mut();
			cur.prev(&mut (*self.backend))?;
		}

		Ok(())
	}

	fn set_program(&mut self, name: &str) -> io::Result<()> {
		if let Some(prog_rc) = self.programs.get(name) {
			self.cur = Some(Rc::<RefCell<dyn PlayProgram>>::downgrade(prog_rc));
		}

		if let Some(cur_weak) = &self.cur {
			let cur_rc = Weak::upgrade(cur_weak).unwrap();
			let mut cur = cur_rc.borrow_mut();
			cur.start(&mut (*self.backend))?;
		}

		Ok(())
	}
}

impl linux_inputdev::InputEventRecv for Player {
	fn consume_input(&mut self, code: u32, value: i32) -> io::Result<bool> {
		if value != 1 {
			return Ok(true);
		}

		if let Some(action) = self.actions.get(&code) {
			let action2 = action.clone();
			match action2 {
				PlayerAction::Play => {
					self.backend.play()?;
				}
				PlayerAction::Stop => {
					self.backend.stop()?;
				}
				PlayerAction::Next => {
					self.next()?;
				}
				PlayerAction::Prev => {
					self.prev()?;
				}
				PlayerAction::VolUp => {
					self.backend.volume_up()?;
				}
				PlayerAction::VolDown => {
					self.backend.volume_down()?;
				}
				PlayerAction::Pause => {
					self.backend.pause()?;
				}
				PlayerAction::SwitchProgram(name) => {
					self.set_program(&name.clone())?;
				}
			}
		} else {
			println!("No action found for key code {}", code);
		}

		Ok(true)
	}
}
